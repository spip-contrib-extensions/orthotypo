<?php

/**
 * Plugin Ortho-Typographie
 * (c) 2013 cedric
 * Licence GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!function_exists('inc_lien')) {
/**
 * pour les liens, on transforme les guill francais &laquo; &raquo; des titres en guill de second niveau
 *
 * @return mixed
 */
function inc_lien(...$args) {
	static $config;
	if (is_null($config)) {
		$config = lire_config('orthotypo/');
	}

	if (!function_exists('inc_lien_dist')) {
		include_spip('inc/lien');
	}
	$typo = inc_lien_dist(...$args);

	// si on n'avait pas precise de titre, changer les guill du titre auto
	if (($config['guillemets'] ?? 'on') && (isset($args[1]) && $args[1] === '' && strpos($typo, '&#171;') !== false)) {
		$typo = preg_replace(',&#171;(&nbsp;)?(.*?)(&nbsp;)?&#187;,S', '&#8220;\2&#8221;', $typo);
	}

	// et hop
	return $typo;
}
}
