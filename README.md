# Plugin OrthoTypo

Améliore la typographie dans SPIP et peut effectuer des remplacements automatiques

[Documentation](https://contrib.spip.net/Ortho-typographie)
