# Changelog

## 2.2.0 - 2023-09-12

### Changed

- !6 : En présence du plugin `titres typographiés` Les remplacements de texte sont faits dans tous les champs

### Fixed

- #4 Faire fonctionner l'insertion des espaces fines insécables
- #4 Ne pas remplir les logs de messages verbeux pour rien

## 2.0.0 - 2023-06-04

### Added

- #1 `aria-hidden='true'` autour des points médians (https://legothequeabf.wordpress.com/2017/11/07/recommandations-pour-une-ecriture-inclusive-et-accessible/)

### Changed

- Nécessite SPIP 4.1 minimum (PHP >= 7.4)

### Fixed

- #3 Ne pas casser les échappements de texte de SPIP (4.2) avec la configuration CAPS du plugin
